import { AuthLoginPage } from './app.po';

describe('auth-login App', () => {
  let page: AuthLoginPage;

  beforeEach(() => {
    page = new AuthLoginPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
